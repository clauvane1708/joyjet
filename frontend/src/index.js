import registerServiceWorker from './registerServiceWorker';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import MainLayout from './layout'
import './index.css'
import 'antd/dist/antd.css';
import { configureStore } from './store/index'
import { BrowserRouter } from 'react-router-dom'
import Routes from './routes'

const store = configureStore({})

render(
    <BrowserRouter>
    <MainLayout>
        <Provider store={store}>            
            <Routes />
        </Provider>
    </MainLayout>
    </BrowserRouter>
 ,document.getElementById('root')
);
registerServiceWorker();
