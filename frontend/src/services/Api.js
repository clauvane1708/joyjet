import apisauce from 'apisauce';

const create = (baseURL = 'http://localhost:3001') => {
  const api = apisauce.create({
    baseURL,
    timeout: 15000
  });

  const Quiz = {
      init: () => api.get(`/quiz`),
      pesquisar: ({nome, qtdQuestoes}) => {
        if(!nome && !qtdQuestoes) return api.get(`/quiz`)
        let params = '?'
        params += nome ? `nome=${nome}` : ''
        params += qtdQuestoes ? `${nome ? '&' : ''}qtdQuestoes=${qtdQuestoes}` : ''
        return api.get(`/quiz${params}`)
      },
      salvar: (quiz) => api.post(`/quiz`, quiz),
      editar: (quiz) => api.put(`/quiz/${quiz.id}`, quiz),
      remover: (id) => api.delete(`/quiz/${id}`),
  };

  const Pergunta = {
    perguntas: () => api.get(`/pergunta`),
    quizes: () => api.get(`/quiz`),
    niveis: () => api.get(`/nivel`),
    pesquisar: ({quizId, nivelId}) => {
      if(!quizId && !nivelId) return api.get(`/pergunta`)
      let params = '?'
      params += quizId ? `quizId=${quizId}` : ''
      params += nivelId ? `${quizId ? '&' : ''}nivelId=${nivelId}` : ''
      return api.get(`/pergunta${params}`)
    },
    salvar: (pergunta) => api.post(`/pergunta`, pergunta),
    editar: (pergunta) => api.put(`/pergunta/${pergunta.id}`, pergunta),
    remover: (id) => api.delete(`/pergunta/${id}`),
  };

  return {
    Quiz,
    Pergunta
  }
}

export default {
  create
}
