import React, { Component } from 'react'
import { Card, Table, Popconfirm, Icon, Divider, Button } from 'antd'
import { INSERTING, EDITING } from '../../util/state'

export default class Tabela extends Component {
    
    getExtra = () => {
        return (
            <div>
                <Button type={ "primary"} 
                        onClick={this.prepareInsert}>
                        NOVO
                </Button>
            </div>
        )
    }

    prepareUpdate = (quiz) => {
        this.props.setState(EDITING)
        this.props.setQuiz(quiz)
    }

    prepareInsert = () => {
        this.props.setState(INSERTING)
        this.props.setQuiz(null)
    }

    render() {
        const { list, remover } = this.props
        return (
                <Card title={ "Listagem" } extra={ this.getExtra()}>
                    <Table rowKey={ (row) => row.id} dataSource={list}>
                        <Table.Column key={'nome'} 
                                      dataIndex={'nome'} 
                                      title={<center>Nome</center>} 
                                      align={ "center" }/>
                        <Table.Column key={'qtdQuestoes'} 
                                      dataIndex={'qtdQuestoes'} 
                                      title={<center>Qtde. de Questões</center>} 
                                      align={ "center" }/>
                        <Table.Column key={'acoes'} 
                                      dataIndex={'acoes'} 
                                      title={<center>Ações</center>} 
                                      align={ "center" }
                                      render={(text, record) => (
                                        <span>
                                            <Popconfirm title={"Você realmente gostaria de remover este item?"}
                                                        placement="topLeft"
                                                        okText={"Sim"}
                                                        okType={"danger"}
                                                        cancelText={"Não"}
                                                        onConfirm={() => remover(record.id)}>
                                                    <Icon type="delete" />	
                                            </Popconfirm>
                                            <Divider type="vertical"/>
                                            <Icon type={ 'edit' } onClick={(e) => this.prepareUpdate(record)}></Icon>
                                        </span>
                                        )}/>
                    </Table>
                </Card>
        )
    }
}