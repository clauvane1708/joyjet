import React, { Component } from 'react'
import { Card, Row, Col, Input, Button, InputNumber } from 'antd'
import { isEqual, isNil } from 'lodash'
import { SEARCHING, INSERTING } from '../../util/state'
import { openNotification } from '../../util/notification'
import { onlyNumbers } from '../../util/helper'

export default class Formulario extends Component {
    
    constructor(props){
        super(props)
        this.state = props.quiz ? { quiz: { ...props.quiz } } : { quiz: {} }
    }

    getExtra = () => {
        return (
            <div>
                <Button type={ "primary"} 
                        onClick={this.voltar}>
                        Voltar
                </Button>
                <Button type={ "primary"} 
                        onClick={ isEqual(this.props.state, INSERTING) ? this.salvar : this.editar}
                        style={{ marginLeft: '10px' }}>
                        { isEqual(this.props.state, INSERTING) ? 'Salvar' : 'Atualizar'}
                </Button>
            </div>
        )
    }

    salvar = () => {
        const { quiz } = this.state
        const { salvar } = this.props
        if(isNil(quiz) || !quiz.nome || !quiz.qtdQuestoes) {
            openNotification({descricao: "Por favor, preencha todos os campos."})
            return
        }
        salvar(quiz)
        this.setState({quiz: {}})
    }

    editar = () => {
        const { quiz } = this.state
        const { editar } = this.props
        if(isNil(quiz) || !quiz.nome || !quiz.qtdQuestoes) {
            openNotification({descricao: "Por favor, preencha todos os campos."})
            return
        }
        editar(quiz)
        this.props.setState(SEARCHING)
    }

    voltar = () => {
        this.props.setState(SEARCHING)
    }

    handleChange = (e, property) => {
        let { quiz } = this.state
        quiz[property] = isEqual(property, 'nome') ? e.target.value : onlyNumbers(e)
        this.setState({quiz})
    }

    keyHandler = (e) => { 
        switch (e.key) {
            case 'Enter':
                isEqual(this.props.state, INSERTING) ? this.salvar() : this.editar()
                break;
            case 'Escape':
                this.voltar();
                break;
            default: break;
        }
    }

    render() {
        let { quiz: { nome, qtdQuestoes }} = this.state
        return (
                <Card title={ "Pesquisa" } 
                      extra={this.getExtra()}
                      style={{ marginBottom: '10px'}}>
                    <Row gutter={ 24 }>
                        <Col span={ 6 }>
                            <label>Nome</label>
                            <Input value={ nome } 
                                   onChange={(e) => this.handleChange(e, 'nome')} 
                                   maxLength={ 100 }
                                   onKeyUp={this.keyHandler}/>
                        </Col>
                        <Col span={ 4 }>
                            <label>Qtde. de Questões</label>
                            <InputNumber value={ qtdQuestoes } 
                                         maxLength={ 2 }
                                         onChange={(e) => this.handleChange(e, 'qtdQuestoes')} 
                                         onKeyUp={this.keyHandler}/>
                        </Col>
                    </Row>
                </Card>
        )
    }
}