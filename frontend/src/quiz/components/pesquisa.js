import React, { Component } from 'react'
import { Card, Row, Col, Input, Button, InputNumber } from 'antd'
import { isEqual } from 'lodash'
import { onlyNumbers } from '../../util/helper'

export default class Pesquisa extends Component {
    
    constructor(props){
        super(props)
        this.state = { quiz: {}}
    }

    getExtra = () => {
        return (
            <div>
                <Button type={ "primary"} 
                        onClick={this.limpar}>
                        Limpar
                </Button>
                <Button type={ "primary"} 
                        onClick={this.pesquisar}
                        style={{ marginLeft: '10px' }}>
                        Pesquisar
                </Button>
            </div>
        )
    }

    pesquisar = () => {
        const { quiz } = this.state
        const { pesquisar } = this.props
        pesquisar(quiz)
    }

    limpar = () => {
        const quiz = {}
        this.setState({quiz})
        const { pesquisar } = this.props
        pesquisar(quiz)
    }

    handleChange = (e, property) => {
        let { quiz } = this.state
        quiz[property] = isEqual(property, 'nome') ? e.target.value : onlyNumbers(e)
        this.setState({quiz})
    }

    keyHandler = (e) => { 
        switch (e.key) {
            case 'Enter':
                this.pesquisar();
                break;
            case 'Escape':
                this.limpar();
                break;
            default: break;
        }
    }

    render() {
        let { quiz: { nome, qtdQuestoes }} = this.state
        return (
                <Card title={ "Pesquisa" } 
                      extra={this.getExtra()}
                      style={{ marginBottom: '10px'}}>
                    <Row gutter={ 24 }>
                        <Col span={ 6 }>
                            <label>Nome</label>
                            <Input value={ nome } 
                                   onChange={(e) => this.handleChange(e, 'nome')} 
                                   onKeyUp={this.keyHandler}/>
                        </Col>
                        <Col span={ 4 }>
                            <label>Qtde. de Questões</label>
                            <InputNumber value={ qtdQuestoes } 
                                   onChange={(e) => this.handleChange(e, 'qtdQuestoes')} 
                                   onKeyUp={this.keyHandler}/>
                        </Col>
                    </Row>
                </Card>
        )
    }
}