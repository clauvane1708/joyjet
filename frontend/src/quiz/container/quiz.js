import React, { Component } from 'react'
import { connect } from 'react-redux'
import QuizActions from '../redux'
import { Spin } from 'antd'
import { isEmpty, get, isEqual } from 'lodash'
import { openNotification } from '../../util/notification'
import Tabela from '../components/tabela'
import Pesquisa from '../components/pesquisa'
import Formulario from '../components/formulario'
import { SEARCHING, EDITING, INSERTING } from '../../util/state'

class Quiz extends Component {
    
    componentDidMount() {
        this.props.init()
    }

    componentWillReceiveProps(nextProps){
    	const message = get(nextProps, ['message'], "")
    	if (!isEmpty(message)) {
            openNotification(message)
            this.props.cleanMessage()
		   }
    }

    render() {
        const { 
            fetching, 
            list, 
            remover, 
            editar, 
            salvar, 
            pesquisar,
            state,
            setState,
            quiz,
            setQuiz
        } = this.props
        return (
            <Spin spinning={ fetching }>
                {
                    isEqual(state, SEARCHING) &&
                    <div>
                        <Pesquisa pesquisar={ pesquisar }/>
                        <Tabela list={ list } 
                                remover={ remover }                            
                                setState={ setState }
                                setQuiz={ setQuiz }/>
                    </div>
                }
                {
                    (isEqual(state, INSERTING) || isEqual(state, EDITING)) &&
                    <div>
                        <Formulario salvar={ salvar } 
                                    setState={ setState }
                                    editar={ editar }
                                    state={ state }
                                    quiz={ quiz }/>
                    </div>
                }
                
            </Spin>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        ...state.quiz.data,
        fetching: state.quiz.fetching,
        state: state.quiz.state,
        quiz: state.quiz.quiz
    }
}

const mapDispatchToProps = (dispatch) => ({
    init: ()  => dispatch(QuizActions.quizInit()),
    cleanMessage: ()  => dispatch(QuizActions.quizCleanMessage()),
    salvar: (quiz) => dispatch(QuizActions.quizSalvar(quiz)),
    editar: (quiz) => dispatch(QuizActions.quizEditar(quiz)),
    pesquisar: (quiz) => dispatch(QuizActions.quizPesquisar(quiz)),
    remover: (id) => dispatch(QuizActions.quizRemover(id)),
    setState: (state) => dispatch(QuizActions.quizSetState(state)),
    setQuiz: (quiz) => dispatch(QuizActions.quizSetQuiz(quiz)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Quiz)