import { call, put } from 'redux-saga/effects'
import QuizActions from './redux';
import { get } from "lodash";

const msgError  = {tipo: 'error', descricao: "Erro inesperado, por favor, entre em contato com o administrador do sistema."}

export function * fetch (api, action)  {
  try {    
      const response = yield call(api.Quiz.init)
      if (response.ok) {
        const list = get(response, ['data'], {})
        yield put(QuizActions.quizSuccess({list}))
      } else {
        yield put(QuizActions.quizFailure(msgError))
     }
  } catch (ex) {
    yield put(QuizActions.quizFailure(msgError))
  }
}

export function * pesquisar (api, { quiz })  {
  try {    
      const response = yield call(api.Quiz.pesquisar, quiz)
      if (response.ok) {
        const list = get(response, ['data'], {})
        yield put(QuizActions.quizSuccess({list}))
      } else {
        yield put(QuizActions.quizFailure(msgError))
     }
  } catch (ex) {
    yield put(QuizActions.quizFailure(msgError))
  }
}

export function * salvar (api, { quiz })  {
  try {    
      const response = yield call(api.Quiz.salvar, quiz)
      if (response.ok) {
        yield put(QuizActions.quizInit())
        yield put(QuizActions.quizSuccess({
          message: {
            tipo: 'success', descricao: 'Item salvo com sucesso.'
          }
        }))
      } else {
        yield put(QuizActions.quizFailure(msgError))
     }
  } catch (ex) {
    yield put(QuizActions.quizFailure(msgError))
  }
}

export function * editar (api, { quiz })  {
  try {    
      const response = yield call(api.Quiz.editar, quiz)
      if (response.ok) {
        yield put(QuizActions.quizInit())
        yield put(QuizActions.quizSuccess({
          message: {
            tipo: 'success', descricao: 'Item atualizado com sucesso.'
          }
        }))
      } else {
        yield put(QuizActions.quizFailure(msgError))
     }
  } catch (ex) {
    yield put(QuizActions.quizFailure(msgError))
  }
}

export function * remover (api, { id })  {
  try {    
      const response = yield call(api.Quiz.remover, id)
      if (response.ok) {
        yield put(QuizActions.quizInit())
        yield put(QuizActions.quizSuccess({
          message: {
            tipo: 'success', descricao: 'Item removido com sucesso.'
          }
        }))
      } else {
        yield put(QuizActions.quizFailure(msgError))
     }
  } catch (ex) {
    yield put(QuizActions.quizFailure(msgError))
  }
}

