import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { get } from "lodash"
import { SEARCHING } from '../util/state'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
   quizInit: null,
   quizSuccess: ['dados'],
   quizPesquisar: ['quiz'],
   quizFailure: ['msgError'],
   quizCleanMessage: null,
   quizSalvar: ['quiz'],
   quizRemover: ['id'],
   quizEditar: ['quiz'],
   quizSetState: ['state'],
   quizSetQuiz: ['quiz']
});

export const QuizTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    data:  {},
    fetching: false,
    state: SEARCHING,
    quiz: null
});

/* ------------- Reducers ------------- */

export const request = (state) => state.merge({ fetching: true })
export const success = (state, { dados }) =>  {

  let data = {
    list: get(dados, ['list'], get(state.data, ['list'], [])),
    message: get(dados, ['message'], get(state.data, ['message'], [])),
  }

   state = state.merge({fetching: false, data})
   return state
}

export const failure = (state, {msgError}) => {
  return state.merge({fetching: false, data: {...state.data, message: msgError}})
}

export const cleanMessage = (state, action) => state.merge({data: {...state.data, message: ""}})

export const setState = (state, action) => state.merge({state: action.state})
export const setQuiz = (state, { quiz }) => state.merge({quiz})

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.QUIZ_INIT]         : request,
  [Types.QUIZ_SUCCESS]      : success,
  [Types.QUIZ_PESQUISAR]    : request,
  [Types.QUIZ_FAILURE]      : failure,
  [Types.QUIZ_CLEAN_MESSAGE]: cleanMessage,
  [Types.QUIZ_SALVAR]       : request,
  [Types.QUIZ_REMOVER]      : request,
  [Types.QUIZ_EDITAR]       : request,
  [Types.QUIZ_SET_STATE]    : setState,
  [Types.QUIZ_SET_QUIZ]     : setQuiz,
})
