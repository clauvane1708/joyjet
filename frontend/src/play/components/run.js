import React, { Component } from 'react'
import { Card, Radio, Button, Popconfirm, Row } from 'antd'
import { CHOOSING, RESULTING } from '../../util/state'
import { isEqual, cloneDeep, isNil } from 'lodash'
import { openNotification } from '../../util/notification'
import { Count } from './count';

let init = { run: {countErro: 0, countAcerto: 0, countPergunta: 0}}
const alternativas = { "0": "A", "1": "B", "2": "C", "3": "D" }

export default class Run extends Component {

    constructor(props) {
        super(props)
        this.state = cloneDeep(init)
    }

    sair = () => {
        const { setState } = this.props
        this.setState(cloneDeep(init), ()=> setState(CHOOSING))
    }

    responder = () => {
        const { run: { opcaoEscolhida, countPergunta } } = this.state        
        if(isNil(opcaoEscolhida)) {
            openNotification({descricao: "Por favor, selecione uma opção."})
            return
        }

        const { perguntas } = this.props
        const pergunta = perguntas[countPergunta]
        if(isEqual(opcaoEscolhida, pergunta.opcaoCorreta)){
            this.setState((prevState) => {
                return { 
                    run: { ...prevState.run, 
                        countAcerto: prevState.run.countAcerto + 1, 
                        countPergunta: prevState.run.countPergunta + 1,
                        opcaoEscolhida: null
                    } 
                }
            }, () => {
                openNotification({tipo: 'success', duracao: 1, descricao: "Você acertou!"})
                this.finalizar()
            })
        } else {
            this.setState((prevState) => {
                return { 
                    run: { ...prevState.run, 
                        countErro: prevState.run.countErro + 1, 
                        countPergunta: prevState.run.countPergunta + 1,
                        opcaoEscolhida: null
                    } 
                }
            }, () => {
                openNotification({tipo: 'error', duracao: 1, descricao: "Você errou!"})
                this.finalizar()
            })
        }
        
    }

    finalizar = () => {
        const { setState, setResultado, perguntas } = this.props
        const { run: { countPergunta } } = this.state  
        if(isEqual(perguntas.length, countPergunta)) {
            setState(RESULTING)
            setResultado(this.state.run)
        }
    }

    handleChange = (e) => {
        let { run } = this.state
        run['opcaoEscolhida'] = e.target.value
        this.setState({run})
    }

    getExtra = () => {
        let { run } = this.state
        return (
            <Count {...run}/>
        )
    }
    
    render() {
        let { run: { countPergunta, opcaoEscolhida } } = this.state
        const { perguntas = [] } = this.props
        const { 
            descricao, 
            opcoes  
        } = perguntas[countPergunta] ? perguntas[countPergunta] : {}
        return (
            <Card title={ descricao } extra= { this.getExtra() }>
                <Row>
                    <Radio.Group onChange={this.handleChange} value={opcaoEscolhida}>
                        {
                            opcoes && opcoes.map((opcao, index) => <Radio key={index} value={index}>{`${alternativas[index]}) ${opcao}`}</Radio>)
                        }
                    </Radio.Group>
                </Row>
                <Row style={ { marginTop: '10px' } }>
                    <Popconfirm title={"Você realmente gostaria de sair do jogo?"}
                                placement="topLeft"
                                okText={"Sim"}
                                okType={"danger"}
                                cancelText={"Não"}
                                onConfirm={this.sair}>
                            <Button type={ "default"} >Sair</Button>
                    </Popconfirm>
                    <Button type={ "primary"} 
                            onClick={this.responder}
                            style={{ marginLeft: '10px' }}>
                            Responder
                    </Button>
                </Row>
            </Card>
        )
    }
}