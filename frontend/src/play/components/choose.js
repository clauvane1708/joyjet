import React, { Component } from 'react'
import { Card, Row, Col, Select, Button } from 'antd'
import { generateOptions } from '../../util/helper'
import { openNotification } from '../../util/notification'
import { isNil, cloneDeep } from 'lodash'

let init = { play: { quizId: null, nivelId: null } }

export default class Choose extends Component {

    constructor(props) {
        super(props)
        this.state = cloneDeep(init)
    }

    handleChange = (value, property) => {
        let { play } = this.state
        play[property] = value
        this.setState({play})
    }

    start = () => {
        const { start } = this.props
        const { play } = this.state
        if( isNil(play.quizId) || isNil(play.nivelId)){
            openNotification({descricao: "Por favor, preencha todos os campos."})
            return
        }
        start(play)
        this.setState(cloneDeep(init))
    }

    render() {
        let { play: { quizId, nivelId } } = this.state
        const { quizList, niveis } = this.props
        return (
            <Card title={ ""}>
                <Row gutter={ 24 }>
                        <Col span={ 6 }>
                            <label>Quiz</label>
                            <Select value={ quizId } 
                                   onChange={(value) => this.handleChange(value, 'quizId')}>
                                   <Select.Option value={ null }>Selecione</Select.Option>
                                   { generateOptions(quizList) }
                            </Select>
                        </Col>
                        <Col span={ 6 }>
                            <label>Nível</label>
                            <Select value={ nivelId } 
                                    onChange={(value) => this.handleChange(value, 'nivelId')}>
                                    <Select.Option value={ null }>Selecione</Select.Option>
                                   { generateOptions(niveis) }
                            </Select>
                        </Col>
                        <Col span={ 6 }>
                        <br />
                            <Button type={ "primary"} 
                                    onClick={this.start}>
                                    Iniciar
                            </Button>
                        </Col>
                    </Row>
            </Card>
        )
    }
}