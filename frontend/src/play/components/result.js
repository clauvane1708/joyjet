import React, { Component } from 'react'
import { Card, Button, Row } from 'antd'
import { CHOOSING } from '../../util/state'
import { Count } from './count'

export default class Result extends Component {

    reiniciar = () => {
        const { setState } = this.props
        setState(CHOOSING)
    }

    getExtra = () => {
        let { resultado } = this.props
        return (
            <Count {...resultado}/>
        )
    }

    render() {
        const { resultado: { countPergunta, countErro, countAcerto } } = this.props
        return (
            <Card title= { "Resultado" } extra = { this.getExtra() }>
                <Row>
                    <label>Você respondeu <b>{countPergunta}</b> pergunta(s).</label><br/>
                    <label>Você errou <b>{countErro}</b> pergunta(s).</label><br/>
                    <label>Você acertou <b>{countAcerto}</b> pergunta(s).</label>
                </Row>
                <Row style={ { marginTop: '10px' } }>
                    <Button type = { "primary" } onClick={ this.reiniciar }>Reiniciar</Button>
                </Row>
            </Card>
        )
    }
}