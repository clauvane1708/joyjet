import React from 'react'
import { Badge, Divider } from 'antd'

const styleBadge = { backgroundColor: '#fff', color: '#999', boxShadow: '0 0 0 1px #d9d9d9 inset' }

export const Count = ({countErro, countPergunta, countAcerto}) => (
    <div>
        <Badge count={ countErro } showZero/>
        <Divider type={"vertical"} />
        <Badge count={ countPergunta } style={styleBadge} showZero/>
        <Divider type={"vertical"} />
        <Badge count={ countAcerto } style={{ backgroundColor: '#52c41a' }} showZero/>
    </div>
)