import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { get } from "lodash"
import { CHOOSING } from '../util/state';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
   playInit: null,
   playSuccess: ['dados'],
   playFailure: ['msgError'],
   playCleanMessage: null,
   playSetState: ['state'],
   playStart: ['play'],
   playSetResultado: ['resultado'],
});

export const PlayTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    data:  {},
    fetching: false,
    state: CHOOSING,
    resultado: null
});

/* ------------- Reducers ------------- */

export const request = (state) => state.merge({ fetching: true })
export const success = (state, { dados }) =>  {

  let data = {
    perguntas: get(dados, ['perguntas'], get(state.data, ['perguntas'], [])),
    message: get(dados, ['message'], get(state.data, ['message'], {})),
    quizList: get(dados, ['quizList'], get(state.data, ['quizList'], [])),
    niveis: get(dados, ['niveis'], get(state.data, ['niveis'], [])),
  }

   state = state.merge({fetching: false, data})
   return state
}

export const failure = (state, {msgError}) => {
  return state.merge({fetching: false, data: {...state.data, message: msgError}})
}

export const cleanMessage = (state, action) => state.merge({data: {...state.data, message: ""}})

export const setState = (state, action) => state.merge({state: action.state})

export const setResultado = (state, { resultado }) => state.merge({resultado})

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PLAY_INIT]           : request,
  [Types.PLAY_SUCCESS]        : success,
  [Types.PLAY_FAILURE]        : failure,
  [Types.PLAY_CLEAN_MESSAGE]  : cleanMessage,
  [Types.PLAY_SET_STATE]      : setState,
  [Types.PLAY_START]          : request,
  [Types.PLAY_SET_RESULTADO]  : setResultado,
})
