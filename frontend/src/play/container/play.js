import React, { Component } from 'react'
import { connect } from 'react-redux'
import PlayActions from '../redux'
import { Spin } from 'antd'
import { isEmpty, get, isEqual } from 'lodash'
import { openNotification } from '../../util/notification'
import { CHOOSING, PLAYING, RESULTING } from '../../util/state'
import Choose from '../components/choose';
import Run from '../components/run';
import Result from '../components/result';

class Play extends Component {
    
    componentDidMount() {
        this.props.init()
    }

    componentWillReceiveProps(nextProps){
    	const message = get(nextProps, ['message'], "")
    	if (!isEmpty(message)) {
            openNotification(message)
            this.props.cleanMessage()
		   }
    }

    render() {
        const { 
            fetching,
            quizList,
            niveis,
            state,
            start,
            setState,
            perguntas,
            resultado,
            setResultado
        } = this.props
        return (
            <Spin spinning={ fetching }>
                {
                    isEqual(state, CHOOSING) &&
                    <div>
                        <Choose start={ start }
                                quizList ={ quizList }
                                niveis = { niveis }/>
                    </div>
                }
                {
                    (isEqual(state, PLAYING)) &&
                    <Run perguntas = { perguntas }
                         setState = { setState }
                         setResultado = { setResultado }/>
                }
                {
                    (isEqual(state, RESULTING)) &&
                    <Result resultado = { resultado }
                            setState = { setState }/>
                }
            </Spin>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        ...state.play.data,
        fetching: state.play.fetching,
        state: state.play.state,
        resultado: state.play.resultado,
    }
}

const mapDispatchToProps = (dispatch) => ({
    init: ()  => dispatch(PlayActions.playInit()),
    cleanMessage: ()  => dispatch(PlayActions.playCleanMessage()),
    start: (play) => dispatch(PlayActions.playStart(play)),
    setState: (state) => dispatch(PlayActions.playSetState(state)),
    setResultado: (resultado) => dispatch(PlayActions.playSetResultado(resultado)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Play)