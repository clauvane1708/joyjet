import { call, put } from 'redux-saga/effects'
import PlayActions from './redux';
import { get, isEmpty } from "lodash";
import { PLAYING } from '../util/state'
const msgError  = {tipo: 'error', descricao: "Erro inesperado, por favor, entre em contato com o administrador do sistema."}

export function * fetch (api, action)  {
  try {    
      const response = yield call(api.Pergunta.quizes)
      if (response.ok) {
        const quizList = get(response, ['data'], {})
        yield put(PlayActions.playSuccess({quizList}))
      } else {
        yield put(PlayActions.playFailure(msgError))
     }

     const response2 = yield call(api.Pergunta.niveis)
      if (response2.ok) {
        const niveis = get(response2, ['data'], {})
        yield put(PlayActions.playSuccess({niveis}))
      } else {
        yield put(PlayActions.playFailure(msgError))
     }
  } catch (ex) {
    yield put(PlayActions.playFailure(msgError))
  }
}

export function * start (api, { play })  {
  try {    
      const response = yield call(api.Pergunta.pesquisar, play)
      if (response.ok) {
        const perguntas = get(response, ['data'], {})
        yield put(PlayActions.playSuccess({perguntas}))
        if(isEmpty(perguntas))
          yield put(PlayActions.playFailure({descricao: 'Não há perguntas para o filtro informado.'}))
        else 
          yield put(PlayActions.playSetState(PLAYING))
      } else {
        yield put(PlayActions.playFailure(msgError))
     }
  } catch (ex) {
    yield put(PlayActions.playFailure(msgError))
  }
}
