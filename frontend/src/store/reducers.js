import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    quiz: require('../quiz/redux').reducer,
    pergunta: require('../pergunta/redux').reducer,
    play: require('../play/redux').reducer,
});

export default rootReducer;
