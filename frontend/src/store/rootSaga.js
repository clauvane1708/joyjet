import { takeLatest, all } from "redux-saga/effects"
import API from "../services/Api";


/* ------------- Types ------------- */
import { QuizTypes } from '../quiz/redux';
import { PerguntaTypes } from '../pergunta/redux';
import { PlayTypes } from '../play/redux';

/* ------------- Sagas ------------- */
import * as Quiz from '../quiz/sagas';
import * as Pergunta from '../pergunta/sagas';
import * as Play from '../play/sagas';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create();

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
    yield all([
        takeLatest(QuizTypes.QUIZ_INIT, Quiz.fetch, api),
        takeLatest(QuizTypes.QUIZ_SALVAR, Quiz.salvar, api),
        takeLatest(QuizTypes.QUIZ_REMOVER, Quiz.remover, api),
        takeLatest(QuizTypes.QUIZ_EDITAR, Quiz.editar, api),
        takeLatest(QuizTypes.QUIZ_PESQUISAR, Quiz.pesquisar, api),

        takeLatest(PerguntaTypes.PERGUNTA_INIT, Pergunta.fetch, api),
        takeLatest(PerguntaTypes.PERGUNTA_SALVAR, Pergunta.salvar, api),
        takeLatest(PerguntaTypes.PERGUNTA_REMOVER, Pergunta.remover, api),
        takeLatest(PerguntaTypes.PERGUNTA_PESQUISAR, Pergunta.pesquisar, api),
        takeLatest(PerguntaTypes.PERGUNTA_EDITAR, Pergunta.editar, api),

        takeLatest(PlayTypes.PLAY_INIT, Play.fetch, api),
        takeLatest(PlayTypes.PLAY_START, Play.start, api),
    ])
}
