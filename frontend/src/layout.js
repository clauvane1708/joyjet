import React, { Component } from 'react'
import { Layout, Menu, Icon } from 'antd';
import { Link } from 'react-router-dom'

const { Header, Content, Footer, Sider } = Layout;

export default class MainLayout extends Component {
  state = {
    collapsed: false,
  };

  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  }

  render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sider
          collapsible
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
            <Menu.Item key="1">
              <Icon type="home" />
              <span>Home</span>
              <Link to="/" />
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="play-circle" />
              <span>Play</span>
              <Link to="/play" />
            </Menu.Item>
            <Menu.Item key="3">
              <Icon type="book" theme="outlined" />
              <span>Quiz</span>
              <Link to="/quiz" />
            </Menu.Item>
            <Menu.Item key="4">
              <Icon type="file-text" />
              <span>Perguntas</span>
              <Link to="/pergunta" />
            </Menu.Item>
            <Menu.Item key="5">
            <Icon type="user" />
              <span>Sobre</span>
              <Link to="/about" />
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', paddingLeft: '10px', marginBottom: '10px' }} >
            <h1>JOYJET - QUIZ</h1>
          </Header>
          <Content style={{ margin: '0 16px' }}>
            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
              { this.props.children }
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Joyjet Digital Space Agency 2016 ☺
          </Footer>
        </Layout>
      </Layout>
    );
  }
}