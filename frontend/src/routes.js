import React from "react";
import LocaleProvider from 'antd/lib/locale-provider';
import ptBR from 'antd/lib/locale-provider/pt_BR';
import { Switch, Route } from 'react-router'

import Home from './home/home'
import Quiz from './quiz/container/quiz'
import Pergunta from './pergunta/container/pergunta'
import About from './about/about'
import Play from './play/container/play'

const Routes = () => (
        <Switch>
            <LocaleProvider locale={ptBR}>
                <div>
                <Route exact path='/' component={Home} />
                <Route path='/home' component={Home} />
                <Route path='/quiz' component={Quiz} />
                <Route path='/pergunta' component={Pergunta} />
                <Route path='/about' component={About} />
                <Route path='/play' component={Play} />
                </div>
            </LocaleProvider>
        </Switch>
);

export default Routes;