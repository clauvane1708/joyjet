import React, { Component } from 'react'

export default class About extends Component {
    render() {
        return (
            <div>
                <h3>Joyjet</h3>
                <p>
                {
                `Joyjet is a digital product agency. 
                Our expertise is taking the brilliant 
                idea you had in the shower to a full 
                blown branded digital product super fast. 
                Concept to market in six weeks for most projects. 
                We are based in Paris, France and Fortaleza, 
                Brazil and our teams speak five major languages.`
                }
                </p>
            </div>
        )
    }
}