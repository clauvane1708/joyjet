import React from 'react'
import { isNil, isEmpty } from 'lodash'
import { Select } from 'antd'

const Option = Select.Option
export const generateOptions = (elements) => {
    if (isNil(elements) || isEmpty(elements)) return

    return elements.map(({ id, descricao, nome }) => <Option key={id}>{ nome || descricao }</Option>)
}

export const onlyNumbers = (value) => (String(value).replace(/\D/gi,''))