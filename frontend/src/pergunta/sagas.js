import { call, put } from 'redux-saga/effects'
import PerguntaActions from './redux';
import { get } from "lodash";

const msgError  = {tipo: 'error', descricao: "Erro inesperado, por favor, entre em contato com o administrador do sistema."}

export function * fetch (api, action)  {
  try {    
      const response = yield call(api.Pergunta.quizes)
      if (response.ok) {
        const quizList = get(response, ['data'], {})
        yield put(PerguntaActions.perguntaSuccess({quizList}))
      } else {
        yield put(PerguntaActions.perguntaFailure(msgError))
     }

     const response2 = yield call(api.Pergunta.niveis)
      if (response2.ok) {
        const niveis = get(response2, ['data'], {})
        yield put(PerguntaActions.perguntaSuccess({niveis}))
      } else {
        yield put(PerguntaActions.perguntaFailure(msgError))
     }

     const response3 = yield call(api.Pergunta.perguntas)
      if (response3.ok) {
        const perguntas = get(response3, ['data'], {})
        yield put(PerguntaActions.perguntaSuccess({perguntas}))
      } else {
        yield put(PerguntaActions.perguntaFailure(msgError))
     }
  } catch (ex) {
    yield put(PerguntaActions.perguntaFailure(msgError))
  }
}

export function * pesquisar (api, { pergunta })  {
  try {    
      const response = yield call(api.Pergunta.pesquisar, pergunta)
      if (response.ok) {
        const perguntas = get(response, ['data'], {})
        yield put(PerguntaActions.perguntaSuccess({perguntas}))
      } else {
        yield put(PerguntaActions.perguntaFailure(msgError))
     }
  } catch (ex) {
    yield put(PerguntaActions.perguntaFailure(msgError))
  }
}

export function * salvar (api, { pergunta })  {
  try {    
      const response = yield call(api.Pergunta.salvar, pergunta)
      if (response.ok) {
        yield put(PerguntaActions.perguntaPesquisar({}))
        yield put(PerguntaActions.perguntaSuccess({
          message: {
            tipo: 'success', descricao: 'Item salvo com sucesso.'
          }
        }))
      } else {
        yield put(PerguntaActions.perguntaFailure(msgError))
     }
  } catch (ex) {
    yield put(PerguntaActions.perguntaFailure(msgError))
  }
}

export function * editar (api, { pergunta })  {
  try {    
      const response = yield call(api.Pergunta.editar, pergunta)
      if (response.ok) {
        yield put(PerguntaActions.perguntaPesquisar({}))
        yield put(PerguntaActions.perguntaSuccess({
          message: {
            tipo: 'success', descricao: 'Item atualizado com sucesso.'
          }
        }))
      } else {
        yield put(PerguntaActions.perguntaFailure(msgError))
     }
  } catch (ex) {
    yield put(PerguntaActions.perguntaFailure(msgError))
  }
}

export function * remover (api, { id })  {
  try {    
      const response = yield call(api.Pergunta.remover, id)
      if (response.ok) {
        yield put(PerguntaActions.perguntaPesquisar({}))
        yield put(PerguntaActions.perguntaSuccess({
          message: {
            tipo: 'success', descricao: 'Item removido com sucesso.'
          }
        }))
      } else {
        yield put(PerguntaActions.perguntaFailure(msgError))
     }
  } catch (ex) {
    yield put(PerguntaActions.perguntaFailure(msgError))
  }
}

