import React, { Component } from 'react'
import { Card, Table, Popconfirm, Icon, Divider, Button } from 'antd'
import { INSERTING, EDITING } from '../../util/state'

export default class Tabela extends Component {

    getExtra = () => {
        return (
            <div>
                <Button type={ "primary"} 
                        onClick={this.prepareInsert}>
                        NOVO
                </Button>
            </div>
        )
    }

    prepareUpdate = (pergunta) => {
        this.props.setState(EDITING)
        this.props.setPergunta(pergunta)
    }

    prepareInsert = () => {
        this.props.setState(INSERTING)
        this.props.setPergunta(null)
    }

    render() {
        const { perguntas, remover } = this.props
        return (
                <Card title={ "Listagem" } extra={ this.getExtra()}>
                    <Table rowKey={ (row) => row.id} dataSource={perguntas}>
                        <Table.Column key={'descricao'} 
                                      dataIndex={'descricao'} 
                                      title={<center>Descrição</center>} 
                                      align={ "center" }/>
                        <Table.Column key={'nivelId'} 
                                      dataIndex={'nivelId'} 
                                      title={<center>Nível</center>} 
                                      align={ "center" }
                                      render={(text, record) => {
                                          let result
                                          switch (text) {
                                              case "0": result = "Fácil"; break;
                                              case "1": result = "Médio"; break;
                                              case "2": result = "Difícil"; break;
                                          
                                              default: result = "Não Informado"; break;
                                          }

                                          return result
                                      }}/>
                        <Table.Column key={'acoes'} 
                                      dataIndex={'acoes'} 
                                      title={<center>Ações</center>} 
                                      align={ "center" }
                                      render={(text, record) => (
                                        <span>
                                            <Popconfirm title={"Você realmente gostaria de remover este item?"}
                                                        placement="topLeft"
                                                        okText={"Sim"}
                                                        okType={"danger"}
                                                        cancelText={"Não"}
                                                        onConfirm={() => remover(record.id)}>
                                                    <Icon type="delete" />	
                                            </Popconfirm>
                                            <Divider type="vertical"/>
                                            <Icon type={ 'edit' } onClick={(e) => this.prepareUpdate(record)}></Icon>
                                        </span>
                                        )}/>
                    </Table>
                </Card>
        )
    }
}