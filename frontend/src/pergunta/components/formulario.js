import React, { Component } from 'react'
import { Card, Row, Col, Input, Button, Select } from 'antd'
import { isEqual, isNil, isEmpty, cloneDeep, trim } from 'lodash'
import { SEARCHING, INSERTING } from '../../util/state'
import { openNotification } from '../../util/notification'
import { generateOptions } from '../../util/helper';

let init = {
    pergunta: {
        quizId: null,
        nivelId: null,
        opcaoCorreta: null,
        opcoes: []
    }
}

export default class Formulario extends Component {
    
    constructor(props){
        super(props)
        this.state = props.pergunta ? { pergunta: cloneDeep(props.pergunta) } : cloneDeep(init)
    }

    getExtra = () => {
        return (
            <div>
                <Button type={ "primary"} 
                        onClick={this.voltar}>
                        Voltar
                </Button>
                <Button type={ "primary"} 
                        onClick={ isEqual(this.props.state, INSERTING) ? this.salvar : this.editar}
                        style={{ marginLeft: '10px' }}>
                        { isEqual(this.props.state, INSERTING) ? 'Salvar' : 'Atualizar'}
                </Button>
            </div>
        )
    }

    salvar = () => {
        const { pergunta } = this.state
        const { salvar } = this.props
        if(!this.isValid(pergunta)) return
        salvar(pergunta)
        this.setState(cloneDeep(init))
    }

    isValid = (pergunta)=> {
        if(isNil(pergunta.descricao) || isNil(pergunta.quizId) || isNil(pergunta.nivelId) || 
            this.isOpcoesInvalidas(pergunta.opcoes) || isNil(pergunta.opcaoCorreta) ) {
            openNotification({descricao: "Por favor, preencha todos os campos."})
            return false
        }

        return true
    }

    isOpcoesInvalidas = (opcoes)=> {
        if(!opcoes || isEmpty(opcoes)) return true

        let result = false
        opcoes.forEach(opcao => {
            console.log(isEmpty(trim(opcao)))
            if(isNil(opcao) || isEmpty(trim(opcao))) {
                result = true;
                return
            }
        });

        return result
    } 

    editar = () => {
        const { pergunta } = this.state
        const { editar } = this.props
        if(!this.isValid(pergunta)) return
        editar(pergunta)
        this.props.setState(SEARCHING)
    }

    voltar = () => {
        this.props.setState(SEARCHING)
    }

    handleChange = (e, property) => {
        let { pergunta } = this.state
        pergunta[property] = e.target.value
        this.setState({pergunta})
    }

    handleChangeOpcoes = (e, property) => {
        let { pergunta } = this.state
        pergunta['opcoes'][property] = e.target.value
        this.setState({pergunta})
    }

    handleChangeSelect = (value, property) => {
        let { pergunta } = this.state
        pergunta[property] = value
        this.setState({pergunta})
    }

    keyHandler = (e) => { 
        switch (e.key) {
            case 'Enter':
                isEqual(this.props.state, INSERTING) ? this.salvar() : this.editar()
                break;
            case 'Escape':
                this.voltar();
                break;
            default: break;
        }
    }

    render() {
        let { 
            pergunta: { 
                quizId, 
                nivelId,
                descricao,
                opcoes,
                opcaoCorreta
            }
        } = this.state
        const { quizList, niveis} = this.props
        return (
                <Card title={ "Pesquisa" } 
                      extra={this.getExtra()}
                      style={{ marginBottom: '10px'}}>
                    <Row gutter={ 24 }>                        
                        <Col span={ 6 }>
                            <label>Quiz</label>
                            <Select value={ quizId } 
                                   onChange={(value) => this.handleChangeSelect(value, 'quizId')}>
                                   <Select.Option value={ null }>Selecione</Select.Option>
                                   { generateOptions(quizList) }
                            </Select>
                        </Col>
                        <Col span={ 6 }>
                            <label>Nível</label>
                            <Select value={ nivelId } 
                                    onChange={(value) => this.handleChangeSelect(value, 'nivelId')}>
                                    <Select.Option value={ null }>Selecione</Select.Option>
                                   { generateOptions(niveis) }
                            </Select>
                        </Col>                        
                        <Col span={ 12 }>
                            <label>Descrição</label>
                            <Input value={ descricao } 
                                onChange={(e) => this.handleChange(e, 'descricao')} 
                                maxLength={ 100 }
                                onKeyUp={this.keyHandler}/>
                        </Col>
                    </Row>
                    <Row gutter={ 24 }>  
                        <Col span={ 12 }>
                            <label>Opção A</label>
                            <Input value={ opcoes[0] } 
                                   onChange={(e) => this.handleChangeOpcoes(e, '0')} 
                                   maxLength={ 100 }
                                   onKeyUp={this.keyHandler}/>
                        </Col>
                        <Col span={ 12 }>
                            <label>Opção B</label>
                            <Input value={ opcoes[1] } 
                                   onChange={(e) => this.handleChangeOpcoes(e, '1')} 
                                   maxLength={ 100 }
                                   onKeyUp={this.keyHandler}/>
                        </Col>
                    </Row>
                    <Row gutter={ 24 }>  
                        <Col span={ 12 }>
                            <label>Opção C</label>
                            <Input value={ opcoes[2] } 
                                   onChange={(e) => this.handleChangeOpcoes(e, '2')} 
                                   maxLength={ 100 }
                                   onKeyUp={this.keyHandler}/>
                        </Col>
                        <Col span={ 12 }>
                            <label>Opção D</label>
                            <Input value={ opcoes[3] } 
                                   onChange={(e) => this.handleChangeOpcoes(e, '3')} 
                                   maxLength={ 100 }
                                   onKeyUp={this.keyHandler}/>
                        </Col>
                    </Row>
                    <Row gutter={ 24 }>  
                        <Col span={ 4 }>
                            <label>Opção Correta</label>
                            <Select value={ opcaoCorreta } 
                                   onChange={(value) => this.handleChangeSelect(value, 'opcaoCorreta')}>
                                   <Select.Option value={ null }>Selecione</Select.Option>
                                   <Select.Option value={ 0 }>A</Select.Option>
                                   <Select.Option value={ 1 }>B</Select.Option>
                                   <Select.Option value={ 2 }>C</Select.Option>
                                   <Select.Option value={ 3 }>D</Select.Option>
                            </Select>
                        </Col>
                    </Row>
                </Card>
        )
    }
}