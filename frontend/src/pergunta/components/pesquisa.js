import React, { Component } from 'react'
import { Card, Row, Col, Button, Select } from 'antd'
import { generateOptions } from '../../util/helper'
import { cloneDeep } from 'lodash'

let init = { pergunta: { quizId: null, nivelId: null } }

export default class Pesquisa extends Component {
    
    constructor(props){
        super(props)
        this.state = cloneDeep(init)
    }

    getExtra = () => {
        return (
            <div>
                <Button type={ "primary"} 
                        onClick={this.limpar}>
                        Limpar
                </Button>
                <Button type={ "primary"} 
                        onClick={this.pesquisar}
                        style={{ marginLeft: '10px' }}>
                        Pesquisar
                </Button>
            </div>
        )
    }

    pesquisar = () => {
        const { pergunta } = this.state
        const { pesquisar } = this.props
        pesquisar(pergunta)
    }

    limpar = () => {
        this.setState(cloneDeep(init), () => {
            const { pesquisar } = this.props
            const { pergunta } = this.state
            pesquisar(pergunta)
        })
        
    }

    handleChange = (value, property) => {
        let { pergunta } = this.state
        pergunta[property] = value
        this.setState({pergunta})
    }

    render() {
        let { pergunta: { quizId, nivelId }} = this.state
        const { quizList, niveis } = this.props 
        return (
                <Card title={ "Pesquisa" } 
                      extra={this.getExtra()}
                      style={{ marginBottom: '10px'}}>
                    <Row gutter={ 24 }>
                        <Col span={ 6 }>
                            <label>Quiz</label>
                            <Select value={ quizId } 
                                   onChange={(value) => this.handleChange(value, 'quizId')}>
                                   <Select.Option value={ null }>Selecione</Select.Option>
                                   { generateOptions(quizList) }
                            </Select>
                        </Col>
                        <Col span={ 6 }>
                            <label>Nível</label>
                            <Select value={ nivelId } 
                                    onChange={(value) => this.handleChange(value, 'nivelId')}>
                                    <Select.Option value={ null }>Selecione</Select.Option>
                                   { generateOptions(niveis) }
                            </Select>
                        </Col>
                    </Row>
                </Card>
        )
    }
}