import React, { Component } from 'react'
import { connect } from 'react-redux'
import PerguntaActions from '../redux'
import { Spin } from 'antd'
import { isEmpty, get, isEqual } from 'lodash'
import { openNotification } from '../../util/notification'
import Tabela from '../components/tabela'
import Pesquisa from '../components/pesquisa'
import Formulario from '../components/formulario'
import { SEARCHING, EDITING, INSERTING } from '../../util/state'

class Pergunta extends Component {
    
    componentDidMount() {
        this.props.init()
    }

    componentWillReceiveProps(nextProps){
    	const message = get(nextProps, ['message'], "")
    	if (!isEmpty(message)) {
            openNotification(message)
            this.props.cleanMessage()
		   }
    }

    render() {
        const { 
            fetching, 
            perguntas, 
            remover, 
            editar, 
            salvar, 
            pesquisar,
            state,
            setState,
            pergunta,
            setPergunta,
            quizList,
            niveis
        } = this.props
        return (
            <Spin spinning={ fetching }>
                {
                    isEqual(state, SEARCHING) &&
                    <div>
                        <Pesquisa pesquisar={ pesquisar }
                                  quizList={ quizList }
                                  niveis={ niveis }/>
                        <Tabela perguntas={ perguntas } 
                                remover={ remover }                            
                                setState={ setState }
                                setPergunta={ setPergunta }/>
                    </div>
                }
                {
                    (isEqual(state, INSERTING) || isEqual(state, EDITING)) &&
                    <div>
                        <Formulario salvar={ salvar } 
                                    setState={ setState }
                                    editar={ editar }
                                    state={ state }
                                    pergunta={ pergunta }
                                    quizList={ quizList }
                                    niveis={ niveis }/>
                    </div>
                }
                
            </Spin>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        ...state.pergunta.data,
        fetching: state.pergunta.fetching,
        state: state.pergunta.state,
        pergunta: state.pergunta.pergunta
    }
}

const mapDispatchToProps = (dispatch) => ({
    init: ()  => dispatch(PerguntaActions.perguntaInit()),
    cleanMessage: ()  => dispatch(PerguntaActions.perguntaCleanMessage()),
    salvar: (pergunta) => dispatch(PerguntaActions.perguntaSalvar(pergunta)),
    editar: (pergunta) => dispatch(PerguntaActions.perguntaEditar(pergunta)),
    pesquisar: (pergunta) => dispatch(PerguntaActions.perguntaPesquisar(pergunta)),
    remover: (id) => dispatch(PerguntaActions.perguntaRemover(id)),
    setState: (state) => dispatch(PerguntaActions.perguntaSetState(state)),
    setPergunta: (pergunta) => dispatch(PerguntaActions.perguntaSetPergunta(pergunta)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Pergunta)