import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { get } from "lodash"
import { SEARCHING } from '../util/state';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
   perguntaInit: null,
   perguntaSuccess: ['dados'],
   perguntaPesquisar: ['pergunta'],
   perguntaFailure: ['msgError'],
   perguntaCleanMessage: null,
   perguntaSalvar: ['pergunta'],
   perguntaRemover: ['id'],
   perguntaCleanTable: null,
   perguntaEditar: ['pergunta'],
   perguntaSetState: ['state'],
   perguntaSetPergunta: ['pergunta']
});

export const PerguntaTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    data:  {},
    fetching: false,
    state: SEARCHING,
    pergunta: null
});

/* ------------- Reducers ------------- */

export const request = (state) => state.merge({ fetching: true })
export const success = (state, { dados }) =>  {

  let data = {
    perguntas: get(dados, ['perguntas'], get(state.data, ['perguntas'], [])),
    message: get(dados, ['message'], get(state.data, ['message'], {})),
    quizList: get(dados, ['quizList'], get(state.data, ['quizList'], [])),
    niveis: get(dados, ['niveis'], get(state.data, ['niveis'], [])),
  }

   state = state.merge({fetching: false, data})
   return state
}

export const failure = (state, {msgError}) => {
  return state.merge({fetching: false, data: {...state.data, message: msgError}})
}

export const cleanMessage = (state, action) => state.merge({data: {...state.data, message: ""}})
export const cleanTable = (state, action) => state.merge({data: {...state.data, perguntas: []}})

export const setState = (state, action) => state.merge({state: action.state})
export const setPergunta = (state, { pergunta }) => state.merge({pergunta})

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PERGUNTA_INIT]           : request,
  [Types.PERGUNTA_SUCCESS]        : success,
  [Types.PERGUNTA_PESQUISAR]      : request,
  [Types.PERGUNTA_FAILURE]        : failure,
  [Types.PERGUNTA_CLEAN_MESSAGE]  : cleanMessage,
  [Types.PERGUNTA_SALVAR]         : request,
  [Types.PERGUNTA_REMOVER]        : request,
  [Types.PERGUNTA_CLEAN_TABLE]    : cleanTable,
  [Types.PERGUNTA_EDITAR]         : request,
  [Types.PERGUNTA_SET_STATE]      : setState,
  [Types.PERGUNTA_SET_PERGUNTA]   : setPergunta,
})
