# joyjet - Quiz

## Passo a passo para subir o projeto

1. Abra o seu terminal
2. Entre na pasta backend
3. Baixe as dependências através do comando 'npm i'
4. Execute o comando 'npm  start'
5. Entre na pasta frontend
6. Baixe as dependências através do comando 'npm i'
7. Execute o comando 'npm  start'
8. Cadastre um Quiz
9. Cadastre Perguntas para o Quiz criado
10. Pronto! É só clicar no 'play' para jogar